package de.ba_dresden.wetterapp.viewmodels;

import java.util.ArrayList;

import de.ba_dresden.wetterapp.models.Forecast;

/**
 * ViewModel for Current Weather Data
 */
public class WeatherViewModel {

    private String temperature;
    private String location;
    private String weatherDescription;
    private String windspeed;
    private String humidity;
    private String rainfall;
    private int imageId;
    private boolean failed;
    private ArrayList<Forecast> forecastTodayList;
    private ArrayList<Forecast> forecastTomorrowList;
    private ArrayList<Forecast> forecastAfterTomorrowList;

    public WeatherViewModel() { }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public void setWeatherDescription(String weatherDescription) {
        this.weatherDescription = weatherDescription;
    }

    public void setWindspeed(String windspeed) {
        this.windspeed = windspeed;
    }

    public void setHumidity(String humidity) {
        this.humidity = humidity;
    }

    public void setRainfall(String rainfall) {
        this.rainfall = rainfall;
    }

    public void setImageId(int imageId) {
        this.imageId = imageId;
    }

    public String getRainfall() {
        return rainfall;
    }

    public int getImageId() {
        return imageId;
    }

    public String getHumidity() {
        return humidity;
    }

    public String getWindspeed() {
        return windspeed;
    }

    public String getWeatherDescription() {
        return weatherDescription;
    }

    public String getLocation() {
        return location;
    }

    public String getTemperature() {
        return temperature;
    }

    public boolean isFailed() {
        return failed;
    }

    public void setFailed(boolean failed) {
        this.failed = failed;
    }

    public ArrayList<Forecast> getForecastTodayList() {
        return forecastTodayList;
    }

    public void setForecastTodayList(ArrayList<Forecast> forecastTodayList) {
        this.forecastTodayList = forecastTodayList;
    }

    public ArrayList<Forecast> getForecastTomorrowList() {
        return forecastTomorrowList;
    }

    public void setForecastTomorrowList(ArrayList<Forecast> forecastTomorrowList) {
        this.forecastTomorrowList = forecastTomorrowList;
    }

    public ArrayList<Forecast> getForecastAfterTomorrowList() {
        return forecastAfterTomorrowList;
    }

    public void setForecastAfterTomorrowList(ArrayList<Forecast> forecastAfterTomorrowList) {
        this.forecastAfterTomorrowList = forecastAfterTomorrowList;
    }
}
