package de.ba_dresden.wetterapp.providers;

import android.util.Log;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import de.ba_dresden.wetterapp.R;
import de.ba_dresden.wetterapp.helpers.WeatherIconHelper;
import de.ba_dresden.wetterapp.models.Forecast;
import de.ba_dresden.wetterapp.models.List;
import de.ba_dresden.wetterapp.models.OpenWeatherResult;
import de.ba_dresden.wetterapp.viewmodels.WeatherViewModel;

public class WeatherProvider {

    public static WeatherViewModel getWeatherViewModel(WeatherViewModel viewModel, OpenWeatherResult result) {

        List weatherNow = result.getList().get(0);
        String location = result.getCity().getName();
        viewModel.setLocation(location);

        String temperature = weatherNow.getMain().getCelsiusTemp();
        viewModel.setTemperature(temperature);

        String weatherDescription = weatherNow.getWeather().get(0).getDescription();
        viewModel.setWeatherDescription(weatherDescription);

        String windspeed = String.format("Windspeed: %s m/s", weatherNow.getWind().getSpeed());
        viewModel.setWindspeed(windspeed);

        String humidity = String.format("Humidity: %s %%", weatherNow.getMain().getHumidity());
        viewModel.setHumidity(humidity);

        // TODO getResources ?
        String rainfall = (weatherNow.getRain() == null ? "no rain in the last 3 hours" : String.format("Rain: %s mm/3h", weatherNow.getRain().get3h()));
        viewModel.setRainfall(rainfall);

        // Converts Weather Icon code to Drawable ID
        int iconId = WeatherIconHelper.getWeatherIconId(weatherNow.getWeather().get(0).getId(), false);
        viewModel.setImageId(iconId);

        viewModel = parseForecast(viewModel, result);

        // Everything was successful, set flag
        viewModel.setFailed(false);

        return viewModel;
    }

    private static WeatherViewModel parseForecast(WeatherViewModel viewModel, OpenWeatherResult result){

        long dayinmillis = 24 * 60 * 60 * 1000;
        long now = System.currentTimeMillis();
        Date dateToday = new Date(now);
        Date dateTomorrow = new Date(now+dayinmillis);
        Date dateDayAfterTomorrow = new Date(now+dayinmillis*2);

        DateFormat dateFormatter = new SimpleDateFormat("yyyy-MM-dd");

        ArrayList<Forecast> todayForecast = new ArrayList<>();
        ArrayList<Forecast> tomorrowForecast = new ArrayList<>();
        ArrayList<Forecast> afterTommorowForecast = new ArrayList<>();

        int weatherIconId;

        // iterate through whole list to get the needed dates
        for (List l : result.getList()) {

            StringBuilder sbTime = new StringBuilder(l.getDtTxt());
            // delete the date from the string
            String time = sbTime.delete(0, 10).toString();
            StringBuilder sbForHandM = new StringBuilder(time);
            // delete the seconds from the srting to show only the hours and minutes
            String hoursMinutes = sbForHandM.delete(6,9).toString();

            int weatherStatusList = l.getWeather().get(0).getId();

            // Converts Weather Icon code to Drawable ID
            int weatherDrawableIcon = WeatherIconHelper.getWeatherIconId(weatherStatusList, true);

            if (weatherDrawableIcon != 0)
                weatherIconId = weatherDrawableIcon;
            else
                weatherIconId = android.R.color.transparent;

            Forecast f = new Forecast(
                    hoursMinutes,
                    String.valueOf(l.getMain().getTemp().intValue()),
                    l.getWeather().get(0).getDescription(),
                    weatherIconId);

            if (l.getDtTxt().contains(dateFormatter.format(dateToday))) {
                Log.d("showForecast", "" + l.getDtTxt());
                todayForecast.add(f);

            } else if (l.getDtTxt().contains(dateFormatter.format(dateTomorrow))) {
                Log.d("showForecast", "" + l.getDtTxt());
                tomorrowForecast.add(f);

            } else if (l.getDtTxt().contains(dateFormatter.format(dateDayAfterTomorrow))) {
                Log.d("showForecast", "" + l.getDtTxt());
                afterTommorowForecast.add(f);
            }
        }

        viewModel.setForecastTodayList(todayForecast);
        viewModel.setForecastTomorrowList(tomorrowForecast);
        viewModel.setForecastAfterTomorrowList(afterTommorowForecast);

        return viewModel;
    }
}
