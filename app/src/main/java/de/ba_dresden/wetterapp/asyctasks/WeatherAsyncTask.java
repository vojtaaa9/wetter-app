package de.ba_dresden.wetterapp.asyctasks;

import android.os.AsyncTask;
import android.util.Log;

import com.google.gson.Gson;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;

import javax.net.ssl.HttpsURLConnection;

import de.ba_dresden.wetterapp.AsyncResponse;
import de.ba_dresden.wetterapp.models.OpenWeatherResult;
import de.ba_dresden.wetterapp.providers.WeatherProvider;
import de.ba_dresden.wetterapp.viewmodels.WeatherViewModel;

public class WeatherAsyncTask extends AsyncTask<Void, OpenWeatherResult, WeatherViewModel> {

    private String weatherUrl;
    private AsyncResponse delegate = null;

    //Constructor to get passed values, Context to obtain View, activity to end SplashScreenActivity
    public WeatherAsyncTask(String url, AsyncResponse delegate) {
        weatherUrl = url;
        this.delegate = delegate;
    }

    // Method which runs on Background Thread
    @Override
    protected WeatherViewModel doInBackground(Void... params) {
        WeatherViewModel result = new WeatherViewModel();
        result.setFailed(true);

        // Try to download JSON File
        try {
            // Use real url address
            HttpURLConnection urlConnection = downloadJSONOnHttp(weatherUrl);

            // Get the InputStream, which holds the JSON response
            InputStream inputStream = urlConnection.getInputStream();

            // Check if inputStream isn't empty and parse it's content
            if (inputStream != null) {
                // Parse the input using GSON library
                OpenWeatherResult openWeatherResult = parseInput(inputStream);
                inputStream.close();
                urlConnection.disconnect();

                result = WeatherProvider.getWeatherViewModel(result, openWeatherResult);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

        // You can also return something else, but than you must change Type in onPostExecute
        return result;
    }

    @Override
    protected void onPostExecute(WeatherViewModel result) {

        delegate.processResults(result);
    }

    private HttpURLConnection downloadJSONOnHttp(String url) throws IOException {

        HttpURLConnection urlConnection = (HttpURLConnection) new URL(url).openConnection();
        urlConnection.setRequestMethod("GET");

        // Set UrlConnection to get InputStream (important to get the file as response)
        urlConnection.setDoInput(true);

        // UrlConnection will follow redirects
        urlConnection.setInstanceFollowRedirects(true);
        urlConnection.connect();

        int responseCode = urlConnection.getResponseCode();
        Log.d("asynctask", "urlConnection response Code: " + responseCode);
        if (responseCode != HttpsURLConnection.HTTP_OK) {
            throw new IOException("HTTP error code: " + responseCode);
        }

        // Return created connection
        return urlConnection;
    }

    private OpenWeatherResult parseInput(InputStream input) throws UnsupportedEncodingException {

        // Use GSON to parse the input
        Gson gson = new Gson();

        Reader inputStreamReader = new InputStreamReader(input, "UTF-8");

        return gson.fromJson(inputStreamReader, OpenWeatherResult.class);
    }
}