package de.ba_dresden.wetterapp;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import de.ba_dresden.wetterapp.models.Forecast;

public class ForecastArrayAdapter extends ArrayAdapter<Forecast> {

    private Context context;

    public ForecastArrayAdapter(Context context, int textViewResourceId, List<Forecast> objects) {
        super(context, textViewResourceId, objects);
        this.context = context;
    }

    /**
     * Forecast View holder holds references to Views from forecast_row.
     * They can be therefore reused in ListView.
     */
    private static class ForecastViewHolder {
        ImageView forecastIcon;
        TextView forecastTime;
        TextView forecastDescription;
        TextView forecastTemperature;
    }

    @NonNull
    public View getView(int position, View convertView, @NonNull ViewGroup parent) {

        View rowView = convertView;

        // If no view has been inflated, do this now
        if (rowView == null) {
            LayoutInflater mInflater = (LayoutInflater) context
                    .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

            rowView = mInflater.inflate(R.layout.forecast_row, null);

            ForecastViewHolder holder = new ForecastViewHolder();

            holder.forecastTime = (TextView) rowView.findViewById(R.id.forecast_time);
            holder.forecastDescription = (TextView) rowView.findViewById(R.id.forecast_desc);
            holder.forecastTemperature = (TextView) rowView.findViewById(R.id.forecast_temp);
            holder.forecastIcon = (ImageView) rowView.findViewById(R.id.forecast_image);

            rowView.setTag(holder);
        }

        ForecastViewHolder holder = (ForecastViewHolder) rowView.getTag();
        Forecast rowItem = getItem(position);

        holder.forecastDescription.setText(rowItem.getDescription());
        holder.forecastTime.setText(rowItem.getTime());
        holder.forecastTemperature.setText(rowItem.getTemperature());
        holder.forecastIcon.setImageResource(rowItem.getImage());

        return rowView;
    }
}
