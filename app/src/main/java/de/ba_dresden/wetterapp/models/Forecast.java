package de.ba_dresden.wetterapp.models;

public class Forecast {

    private String time;
    private String temperature;
    private String description;
    private int image;

    public Forecast(String time, String temperature, String description, int image) {
        this.time = time;
        this.temperature = temperature;
        this.description = description;
        this.image = image;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getTemperature() {
        return temperature + " °C";
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }
}
