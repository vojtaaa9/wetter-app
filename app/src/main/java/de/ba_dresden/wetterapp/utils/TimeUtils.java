package de.ba_dresden.wetterapp.utils;

import java.text.SimpleDateFormat;

public class TimeUtils {

    public static String convertTimestampToEnglishDate(long timestamp) {

        SimpleDateFormat sdf = new SimpleDateFormat("EEE dd.MM");

        return sdf.format(timestamp);
    }

    public static String convertTimestampToHours(long timestamp) {

        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm");

        return sdf.format(timestamp);
    }

}
