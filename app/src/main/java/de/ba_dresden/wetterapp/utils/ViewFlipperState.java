package de.ba_dresden.wetterapp.utils;

public enum ViewFlipperState {
    ERROR,
    FIND_LOCATION,
    SHOW_DATA
}