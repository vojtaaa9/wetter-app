package de.ba_dresden.wetterapp.utils;

import android.util.Log;
import android.widget.ViewFlipper;

public class ViewFlipperManager {

    private ViewFlipper viewFlipper;
    private ViewFlipperState state;

    public ViewFlipperManager(ViewFlipper viewFlipper){
        this.viewFlipper = viewFlipper;
        state = ViewFlipperState.values()[viewFlipper.getDisplayedChild()];
        Log.d(this.toString(), "Current ViewFlipperManagerState: " + state);
    }

    public int getCurrentChild(){
        return viewFlipper.getDisplayedChild();
    }

    /**
     * Shows a flip from ViewFlipper. States corresponds to its position.
     * @param state Desired state
     * @return whether the screen was switched or not. False means the
     */
    public boolean showScreen(ViewFlipperState state) {

        // If current state is already the state which should be switched, do nothing and return.
        if (state == this.state)
            return false;

        // Flip to desired child
        viewFlipper.setDisplayedChild(state.ordinal());
        this.state = state;
        return true;
    }


}
