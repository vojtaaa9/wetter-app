package de.ba_dresden.wetterapp;

import de.ba_dresden.wetterapp.viewmodels.WeatherViewModel;

public interface AsyncResponse {
    void processResults(WeatherViewModel result);
}
