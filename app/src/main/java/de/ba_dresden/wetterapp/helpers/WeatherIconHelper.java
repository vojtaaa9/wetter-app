package de.ba_dresden.wetterapp.helpers;

import de.ba_dresden.wetterapp.R;

public class WeatherIconHelper {

    /**
     * Converts Weather Icon Id from API to R.drawable IDs
     * @param iconId Weather Icon Id
     * @param small Boolean, indication whether the icon should be small or medium size
     * @return R.drawable Id
     */
    public static int getWeatherIconId(int iconId, boolean small){

        if (iconId >= 200 && iconId < 300 )
            return small ? R.drawable.ic_weather_storm_sm : R.drawable.ic_weather_storm_md;
        if (iconId >= 300 && iconId < 500 )
            return small ? R.drawable.ic_weather_cloud_sm : R.drawable.ic_weather_cloud_md;
        if (iconId >= 500 && iconId < 600)
            return small ? R.drawable.ic_weather_rain_sm : R.drawable.ic_weather_rain_md;
        if (iconId >= 600 && iconId < 700)
            return small ? R.drawable.ic_weather_snow_sm : R.drawable.ic_weather_snow_md;
        if (iconId == 741)
            return small ? R.drawable.ic_weather_fog_sm : R.drawable.ic_weather_fog_md;
        if (iconId == 800)
            return small ? R.drawable.ic_weather_sun_sm : R.drawable.ic_weather_sun_md;
        if (iconId == 801)
            return small ? R.drawable.ic_weather_partly_cloudy_sm : R.drawable.ic_weather_partly_cloudy_md;
        if (iconId >= 802 && iconId <= 804)
            return small ? R.drawable.ic_weather_cloud_sm : R.drawable.ic_weather_cloud_md;
        return 0;
    }
}
