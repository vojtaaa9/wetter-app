package de.ba_dresden.wetterapp.fragments;

import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import de.ba_dresden.wetterapp.ForecastArrayAdapter;
import de.ba_dresden.wetterapp.R;
import de.ba_dresden.wetterapp.models.Forecast;

public class ForecastListFragment extends ListFragment {

    private ForecastArrayAdapter myForecastArrayAdapter;
    private List<Forecast> listData;

    public ForecastListFragment() {
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.forecast_list_fragment, container, false);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        ArrayList<Forecast> forecastItems = new ArrayList<>();

        myForecastArrayAdapter = new ForecastArrayAdapter(getActivity(), android.R.id.list, forecastItems);

        setListAdapter(myForecastArrayAdapter);
    }

    public void setListData(ArrayList<Forecast> list){
        // Set ForecastData to list
        listData = list;

        updateListView();
    }

    private void updateListView() {

        if(listData == null || listData.size() == 0) {
            Log.w(this.toString(), "Missing list data, cannot update Fragment " + ForecastListFragment.class);
            return;
        }

        for (Forecast forecast : listData) {
            myForecastArrayAdapter.add(forecast);
        }

        // Notify Adapter, that the underlying data has changed and i should update itself.
        myForecastArrayAdapter.notifyDataSetChanged();
    }
}
