package de.ba_dresden.wetterapp;

import android.Manifest;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.github.aakira.expandablelayout.ExpandableRelativeLayout;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;

import java.util.List;

import de.ba_dresden.wetterapp.asyctasks.WeatherAsyncTask;
import de.ba_dresden.wetterapp.fragments.ForecastListFragment;
import de.ba_dresden.wetterapp.utils.TimeUtils;
import de.ba_dresden.wetterapp.utils.ViewFlipperManager;
import de.ba_dresden.wetterapp.utils.ViewFlipperState;
import de.ba_dresden.wetterapp.viewmodels.WeatherViewModel;

public class MainActivity extends AppCompatActivity implements AsyncResponse {

    private FusedLocationProviderClient fusedLocationClient;
    final private int MY_PERMISSIONS_REQUEST_LOCATION = 123;
    final private static String API_KEY = "abac53533ce86018c26695a2bf82e9a5";
    final private static String partURL = "http://api.openweathermap.org/data/2.5/forecast?";
    public ViewFlipperManager viewFlipperManager;
    TextView txLocation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        initToggle();

        TabLayout tabLayout = findViewById(R.id.tabLayout);
        ViewPager viewPager = findViewById(R.id.pager);

        // Initialize ViewPager Tabs
        setupViewPager(viewPager);

        tabLayout.setupWithViewPager(viewPager);

        // Sets Text of Tabs. Do this after the TabLayout war setup with ViewPager
        setTabsText(tabLayout);

        // Initialize ViewFlipperManager
        viewFlipperManager = new ViewFlipperManager((ViewFlipper) findViewById(R.id.viewFlipper));
        viewFlipperManager.showScreen(ViewFlipperState.FIND_LOCATION);


        // TODO: @Vojta this is dirty, there should be some kind of better solution
        txLocation = findViewById(R.id.weather_location_now);

        fusedLocationClient = LocationServices.getFusedLocationProviderClient(this);

        checkLocationPermission();
    }

    @Override
    protected void onStart() {
        super.onStart();

    }

    /**
     * Checks if location permissions are granted. If not user will be prompted to do so.
     * Else just gets the Location.
     */
    private void checkLocationPermission() {
        // If permissions aren't granted yet, ask user to do so
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this,
                    new String[]{Manifest.permission.ACCESS_FINE_LOCATION},
                    MY_PERMISSIONS_REQUEST_LOCATION);
        }
        // Location permission is already granted, do the location job
        else
            getLocation();
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[], @NonNull int[] grantResults){
        if(requestCode == MY_PERMISSIONS_REQUEST_LOCATION){
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED)
                getLocation();
        }
    }

    /**
     * Sets up ViewPager with {@link de.ba_dresden.wetterapp.MyFragmentPagerAdapter}. Also creates 3 Fragments
     * @param viewPager ViewPager to set up
     */
    private void setupViewPager(ViewPager viewPager) {
        // Sets offscreen fragment page limit to 2. This will hold all 3 fragments in memory
        // and won't recycle them. Is useful for small set of Fragments.
        viewPager.setOffscreenPageLimit(2);

        MyFragmentPagerAdapter adapter = new MyFragmentPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new ForecastListFragment());
        adapter.addFragment(new ForecastListFragment());
        adapter.addFragment(new ForecastListFragment());
        viewPager.setAdapter(adapter);
    }

    /**
     * Initializes ExpandableLayout and sets OnClickListener for ToggleButton
     */
    private void initToggle() {
        final ImageButton infoToggle = findViewById(R.id.weather_dropdown);
        final ExpandableRelativeLayout expandableLayout = findViewById(R.id.expandableLayout);

        // Layout is expanded in startup state, close it programmatically
        if (expandableLayout.isExpanded()) {
            expandableLayout.collapse(0, null);
        }

        infoToggle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                expandableLayout.toggle();

                float rotation = infoToggle.getRotation();
                infoToggle.setRotation(rotation + 180);
            }
        });
    }

    private void setTabsText(TabLayout tabLayout) {
        long dayInMillis = 24 * 60 * 60 * 1000;
        long now = System.currentTimeMillis();

        TabLayout.Tab tab0 = tabLayout.getTabAt(0);
        TabLayout.Tab tab1 = tabLayout.getTabAt(1);
        TabLayout.Tab tab2 = tabLayout.getTabAt(2);
        if (tab0 != null) {
            tab0.setText(TimeUtils.convertTimestampToEnglishDate(now));
        }
        if (tab1 != null) {
            tab1.setText(TimeUtils.convertTimestampToEnglishDate(now + dayInMillis));
        }
        if (tab2 != null) {
            tab2.setText(TimeUtils.convertTimestampToEnglishDate(now + dayInMillis * 2));
        }
    }

    /**
     * Tries to get last known location of the device.
     * Uses FusedLocationProvider.getLastLocation() which can return null for different reasons.
     * If location was retrieved successfully, starts {@link de.ba_dresden.wetterapp.asyctasks.WeatherAsyncTask}.
     * Else show an Error Screen
     */
    private void getLocation() {

        try {
            fusedLocationClient.getLastLocation()
                    .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            // Got last known location, usually equivalent to the users current location.
                            // In some rare situations this can be null.
                            if (location == null) {
                                Log.d("Location", "Location not available");
                                // Shows error screen
                                showErrorScreen("Location not available");
                                return;
                            }

                            // Build the URL for API request
                            String URL = partURL + "lat=" + String.valueOf(location.getLatitude())
                                    + "&lon=" + String.valueOf(location.getLongitude())
                                    + "&appid=" + API_KEY
                                    + "&units=metric";

                            Log.d("Location", "show whole url: " + URL);

                            // Show ListView tabs
                            viewFlipperManager.showScreen(ViewFlipperState.SHOW_DATA);

                            txLocation.setText(String.format("%s, %s", location.getLatitude(), location.getLongitude()));

                            // Starts the async task to interact with the API
                            WeatherAsyncTask weatherAsyncTask = new WeatherAsyncTask(URL, MainActivity.this);
                            weatherAsyncTask.execute();
                        }
                    }).addOnFailureListener(this, new OnFailureListener() {
                @Override
                public void onFailure(@NonNull Exception e) {
                    e.printStackTrace();
                    // Shows error screen
                    showErrorScreen("Location not available");
                }
            });
        }
        catch (SecurityException e){
            e.printStackTrace();
        }

    }

    /**
     * Shows an ErrorScreen with custom message
     * @param message String Message
     */
    public void showErrorScreen(String message) {
        viewFlipperManager.showScreen(ViewFlipperState.ERROR);
        txLocation.setText(message);

        ImageView img = findViewById(R.id.weather_image_now);
        img.setImageDrawable(null);

        TextView temperature = findViewById(R.id.weather_temp_now);
        temperature.setText("--°C");

        TextView status = findViewById(R.id.weather_status_now);
        status.setText("");
    }

    /**
     * Process the data from Model and shows it on screen
     * @param result OpenWeatherResult containing the Data
     */
    @Override
    public void processResults(WeatherViewModel result) {

        // If no results are received, show error screen
        if (result == null || result.isFailed()) {
            showErrorScreen("Couldn't download data");
            return;
        }

        // success, show the data
        // show the city name
        TextView txWeatherLocationNow = findViewById(R.id.weather_location_now);
        txWeatherLocationNow.setText(result.getLocation());

        // show the current temperature
        TextView txWeatherTemperature = findViewById(R.id.weather_temp_now);
        txWeatherTemperature.setText(result.getTemperature());

        // show the current weather status, for example cloudy or sunny
        TextView txWeatherStatus = findViewById(R.id.weather_status_now);
        txWeatherStatus.setText(result.getWeatherDescription());

        // show the windspeed
        TextView txWeatherWind = findViewById(R.id.weather_wind_status);
        txWeatherWind.setText(result.getWindspeed());

        // show the humidity
        TextView txWeatherHumidity = findViewById(R.id.weather_humidity_status);
        txWeatherHumidity.setText(result.getHumidity());
//        txWeatherHumidity.setText();

        // show the rainfall of the last 3 hours
        TextView txWeatherRainfall = findViewById(R.id.weather_rainfall_status);
        txWeatherRainfall.setText(result.getRainfall());

        // set the weather icon
        ImageView ivWeatherIcon = findViewById(R.id.weather_image_now);

        if (result.getImageId() != 0)
            ivWeatherIcon.setImageResource(result.getImageId());
        else
            ivWeatherIcon.setImageDrawable(null);

        showForecast(result);
    }

    private void showForecast(WeatherViewModel result) {

        // Contains all fragments from fragmentManager, in correct Order
        List fragmentList = getSupportFragmentManager().getFragments();

        int counter = 0;

        // Returns only objects
        for (Object obj: fragmentList) {

            // Cast the object to correct fragment (could throw exception if incorrect casting)
            if (obj.getClass() == ForecastListFragment.class) {

                ForecastListFragment fragment = (ForecastListFragment) obj;

                // Really ugly
                if(counter == 0)
                    fragment.setListData(result.getForecastTodayList());
                else if(counter == 1)
                    fragment.setListData(result.getForecastTomorrowList());
                else if(counter == 2)
                    fragment.setListData(result.getForecastAfterTomorrowList());

                counter++;
            }
        }

    }
}
