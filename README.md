# README #

Go on.

### What is this repository for? ###

* Android App, which shows local weather data.
* Version alpha 0.1

### How do I get set up? ###

* Git clone the repository into android studio
* Run Gradle build command
* Install missing dependencies
* You are ready to go!
